# Pixelhouse Backend Test
by Aris Pujud Kurniawan

## System Requirement
- PHP version 7.0 or latest

## How to run
run via terminal or cmd
```
$ php no_1.php
```

provide your input data when terminal displaying input prompt and then press enter. The output will appear bellow it. 
```
$ php no_2.php
Input: 7
1-2-4-7-11-16-22
```

Thank you for giving me the opportunity to take this test. I am very happy to be able to work with your company.

Best Regards,

Aris Pujud Kurniawan