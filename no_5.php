<?php
/** 
 * Soal no 5
**/
include "Mylib.php";
$pohon = new Mylib\Pohon();

$dt = $pohon->get_pohon();
$input = readline("Input: ");
$input = strtoupper($input);
$input = explode('-', $input);

foreach($input as $in){
    foreach($dt as $d){
        $status = $pohon->search_parent($d, $in);
        if($status){
            $dt = $d['child'];
            break;
        }
    }
    if(!$status) break;
}
if($status){
    $pohon->search_child($dt);
}
else{
    echo "parent tidak sesuai";
}
?>