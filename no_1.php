<?php
/** Soal no 1
*   menggunakan metode Central Polygonal Number.
*   Karena akan digunakan juga pada soal no 2, sehingga dibuat class sendiri
**/
include "Mylib.php";
$cpoly = new Mylib\A000124();

//input 2
$input = 2;
echo $cpoly->result($input);
echo "\n";
//input 20
$input = 20;
echo $cpoly->result($input);
echo "\n";
//input 50
$input = 50;
echo $cpoly->result($input);
echo "\n";
?>