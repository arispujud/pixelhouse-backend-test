<?php
/**
 * Name: Aris Pujud Kurniawan
 * Subject: Pixelhouse - Test [Backend]
 * Tools: PHP CLI
 */
namespace Mylib;
class A000124 {
  public function result($n){
    return $n * ($n+1)/2 + 1;
  }
}

class Pohon{
  public function get_pohon(){
    return [
      [
        'name' => 'A',
        'child' => [
          [
            'name' => 'B',
            'child' => [
              [
                'name' => 'D',
                'child' => []
              ],
              [
                'name' => 'E',
                'child' => []
              ]
            ]
          ],
          [
            'name' => 'C',
            'child' => [
              [
                'name' => 'F',
                'child' => []
              ],
              [
                'name' => 'G',
                'child' => []
              ],
              [
                'name' => 'H',
                'child' => []
              ]
            ]
          ]
        ]
      ],
    ];
  }
  public function show_name($dt,$prefix=""){
    echo $prefix;
    echo $dt['name']."\n";
    $prefix .="-";
    foreach($dt['child'] as $c){
      $this->show_name($c,$prefix);
    }
  }
  public function search_parent($pohon,$input){
    $status = false;
    if($pohon['name'] == $input) $status = true;
    return $status;
  }
  public function search_child($pohon){
    if(count($pohon)>0){
      foreach($pohon as $key=>$p){
        if($key!=0){
          echo "-";
        }
        echo $p['name'];
      }
    }
    else{
      echo "Tidak ada child";
    }
  }
}
?>