<?php
/** Soal no 2
*   menggunakan metode Central Polygonal Number.
*   Karena akan digunakan juga pada soal no 1, sehingga dibuat class sendiri
**/
include "Mylib.php";
$cpoly = new Mylib\A000124();

$input = readline("Input: ");
for($i=0; $i<$input; $i++){
    if($i!=0) echo "-";
    echo $cpoly->result($i);
}
?>